import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
	def test_201(self):
		input = '''
func jT (number qS)	return [not "+'"w'"-"]
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 201))

	def test_202(self):
		input = '''
## &S}8swG
bool CkL[6.723]
## d:p6gc{x!AJqpz{1B#g
## 8+Z%
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 202))

	def test_203(self):
		input = '''
var KDPi[34,6.011E+85] ## Oepf8Y[L4Yrr({jl
'''
		expect = '''Error on line 2 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 203))

	def test_204(self):
		input = '''
bool aO[99.198,5.733E96,7] <- - eMAA and false
func wJhT ()
	begin
	end

var r7I <- false ## ,>5-AF#qS#[ki4E4 JdN
var P_[87,0.504e-14] <- false ## G
'''
		expect = '''Error on line 8 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 204))

	def test_205(self):
		input = '''
bool Rj6S[27.827e84] ## "(N&3JY{hy?.:CGGg
func Wihb (bool v0K, var Nbc[3.071])	return
func L1k8 ()
	begin
		## |Fx<ond>2IKw
		number hj[2.478e+59,60.956e44]
	end

func IE (bool mn, var h0o[556], var bZ[120.482e-49,72.287e-98,239E-91])
	return "'"wn'"`"

'''
		expect = '''Error on line 3 col 21: var'''
		self.assertTrue(TestParser.test(input, expect, 205))

	def test_206(self):
		input = '''
func YsbV (dynamic hrm[45])
	begin
		if (false)
		continue
		## "4sD+.]8l
	end
func iE_ (string SEz7, var zs[3.249,6.506,464.653E69])
	return "'"'"'"'"u"
'''
		expect = '''Error on line 2 col 11: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 206))

	def test_207(self):
		input = '''
bool ZcVc
func UR (string FXb[52e44])
	begin
		W2()
		## oqJ*Yx,BVfvG14XyX%h
		kq["'"'"'"O#", rz, GM8G] <- zro
	end
## &SE{JslAL9dZhjMnMp u
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 207))

	def test_208(self):
		input = '''
number lk[72.689E13,1]
func QZ9 (string aysh[5.606e-91,7.873e+03])
	return

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 208))

	def test_209(self):
		input = '''
## MFd2|]Ue
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 209))

	def test_210(self):
		input = '''
func nen (dynamic Yo, var Pjcb)	begin
		## 8p`%j
	end
## o~cS])!H<m_5
func wlA (string Zbb[757.855,8.969E98], var HgN)	return "'"'"ee"

'''
		expect = '''Error on line 2 col 10: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 210))

	def test_211(self):
		input = '''
string Hl[72E-18,9.353] ## lqr2Nk8f
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 211))

	def test_212(self):
		input = '''
## gKJgm(x|Mo&l
## f)
'''
		expect = '''Error on line 4 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 212))

	def test_213(self):
		input = '''
## 2d%g_%<NsRUN{p#k
func SfH (number YXQ)	return
number twt <- 572E+44 ## Tnmve1r
bool KXA
## ,}S.(X<|Ydd|Rg
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 213))

	def test_214(self):
		input = '''
## o?p]
## q
## SNRB 9qJcE.I;F{
## `Ba
'''
		expect = '''Error on line 6 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 214))

	def test_215(self):
		input = '''
## :uI:EH^GVx-7J)_/T
func YZ6A (number Hgxs)	return 4.447

## W>DmGp6b
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 215))

	def test_216(self):
		input = '''
string Jm <- g_9 ## i6Mpx,op@B<0*Jz3f43
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 216))

	def test_217(self):
		input = '''
dynamic Lz1[33.827,20E+56,7] <- qwt ## +
func sNk ()
	return

bool kHU[5] <- true
number k9y <- false
func LbCh ()
	return FxDB
'''
		expect = '''Error on line 2 col 11: ['''
		self.assertTrue(TestParser.test(input, expect, 217))

	def test_218(self):
		input = '''
string eQM[8.573E11,64.876,2]
var YGb <- false ## ]lTn<~z
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 218))

	def test_219(self):
		input = '''
func cZs (bool cMv[17], bool whPx)
	begin
		return
	end
string o7 ## #
string gO5A[82.961,5.364,954]
## !{0uGUZ4kYd=K>9/"
func OST ()	return true
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 219))

	def test_220(self):
		input = '''
func dkhX (var Z1s, number Zr, string sz[917.845,6.245E-13])
	return true
'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 220))

	def test_221(self):
		input = '''
var ZLO[6] <- 485.516e79
## |..
'''
		expect = '''Error on line 2 col 7: ['''
		self.assertTrue(TestParser.test(input, expect, 221))

	def test_222(self):
		input = '''
func Rpx (number NU[44e-41], var FxSL[98.825,3.140])	return

## A>u3yTS"5do/>([u5wQ
func rS (dynamic ic, number B1)	return LZ

'''
		expect = '''Error on line 2 col 29: var'''
		self.assertTrue(TestParser.test(input, expect, 222))

	def test_223(self):
		input = '''
## >U4_(wBc-L|TmMzd~
## o
string Fl <- 633 ## `Av DL!_E,[db:6U.{=
## /@kKJ[=xTDq
func rB (bool ArI, dynamic mQbu, bool U6[650E59,62.169e30,42E-59])	begin
	end

'''
		expect = '''Error on line 6 col 19: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 223))

	def test_224(self):
		input = '''
func AN0C (string WejB, number pF8)
	return 6

## y,k,&*x2Q`is  L.
func fO ()
	return
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 224))

	def test_225(self):
		input = '''
string IvyH[0E28,47.864] <- M6iY ## 8!hVuBr/~T3HLtTs
number Spd <- "L+'""
func VtoD (string CfN[4,222.027,88])
	return

## b@?v$*FP?B~+qBGnd=AS
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 225))

	def test_226(self):
		input = '''
func iI (number bG[143.067,0.286E35], number Fp_, number GsFn)
	begin
		continue
		bool Zvwo[4.532,176]
	end

func FIg (string LiK[6.552E+95,62,5.170E-80], bool tF)
	return false
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 226))

	def test_227(self):
		input = '''
var UfHd
## kDunlw$1|i
'''
		expect = '''Error on line 2 col 9: 
'''
		self.assertTrue(TestParser.test(input, expect, 227))

	def test_228(self):
		input = '''
bool NZI[123E+57,1.225] <- true ## 5l)%c
## -hmY#=fwZFY}.gO$oN_
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 228))

	def test_229(self):
		input = '''
func jcL (dynamic SgM9, dynamic xB6l[0])
	return 1.737e+66
'''
		expect = '''Error on line 2 col 10: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 229))

	def test_230(self):
		input = '''
## T~)Q.]f[)Yih9S?7z
## 4Sa#
## 9l2TL^ORWU2W
'''
		expect = '''Error on line 5 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 230))

	def test_231(self):
		input = '''
## y
## ZDCk7WgAC4Ou
## FeRZWuYa4I
'''
		expect = '''Error on line 5 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 231))

	def test_232(self):
		input = '''
number iIeh[75.053E+54,8E+52,0.548] ## -MV9
func aJra (number mQn[7,569.594,10.013], bool sE9[28.742,226,831])	begin
		number jc
		## "(+l
		return Q1k
	end

func jl0c (var FMY[359.732], bool Jxu[6])
	begin
		## l3=<Gz)i&
		## z9NM9M:4>%E
		## ;ZEi_Kt}%#Ti^
	end
'''
		expect = '''Error on line 9 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 232))

	def test_233(self):
		input = '''
func He2i (number CSzf[78.035E+28,79.171], dynamic dwP[532,3E+41], bool Tx2[87.903,622.335e-77,916e-45])
	return
## uBqX
bool Tg ## Pvbfzhp^F+1&
var vJB
dynamic SSb <- true
'''
		expect = '''Error on line 2 col 43: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 233))

	def test_234(self):
		input = '''
## k[fPzR
func uW (var Y1pU[50.779E-44,246.919e+20,31e+51], var GZu[9E+37])	return false
## ELh_T
dynamic QJQr <- "'""
'''
		expect = '''Error on line 3 col 9: var'''
		self.assertTrue(TestParser.test(input, expect, 234))

	def test_235(self):
		input = '''
dynamic fC <- DE ## ="&<,lMJ
func x5ui (string dm, number kG, number U8H[3E26])	begin
		## Geb
		## B|HZ/I7w
	end

bool xsQ[1.250e-43,5e+31]
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 235))

	def test_236(self):
		input = '''
## f-s3EYstU{+`;F[2Q
##  -KE1
## O(meS|_Yb_}y2vAjv{k
## U$>lJ X|G
'''
		expect = '''Error on line 6 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 236))

	def test_237(self):
		input = '''
number Mx[0.706E+79]
bool BElC[0.772,422E62,1] <- true ## u8RH
func xmW (var mD8X, number Nc, var dLg3)	return

number v6m <- false ## -/h"x;RZ#
'''
		expect = '''Error on line 4 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 237))

	def test_238(self):
		input = '''
## 7
bool Qws ## anzwQ(j5MtGaQ7>qOY
string KLu
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 238))

	def test_239(self):
		input = '''
func Ks5 (var AxPg)
	return
## F[E=K8W/mSs{
'''
		expect = '''Error on line 2 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 239))

	def test_240(self):
		input = '''
var Zr[4.244]
## uM+s`__
string Dvi[3]
dynamic ezW <- false
'''
		expect = '''Error on line 2 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 240))

	def test_241(self):
		input = '''
dynamic xOu[39E+36,3.986E-18,0]
string ZA1[4,117] <- 3.782
'''
		expect = '''Error on line 2 col 11: ['''
		self.assertTrue(TestParser.test(input, expect, 241))

	def test_242(self):
		input = '''
## 1R-
dynamic wTD ## XwW%b$yg]qz9p,T#
func bfF (var MT[374e90,4.692E-30,6.957e01], string Tob[78.607e79,0.229E85], var SU)	begin
		## &hT0o2lJ<N>9
	end

func sv ()
	begin
	end

dynamic K7b
'''
		expect = '''Error on line 4 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 242))

	def test_243(self):
		input = '''
## [C#kzOxO]?-9OF]D4@7
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 243))

	def test_244(self):
		input = '''
string XE1
## 6+}&O2NWip--?B)h~=`
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 244))

	def test_245(self):
		input = '''
##  X]!8n,2cy
func c1rr (string eQ[2.977E-75])
	return 63
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 245))

	def test_246(self):
		input = '''
## u^]c]
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 246))

	def test_247(self):
		input = '''
## h9R
## CeHeT6[uB L6A|y^,;
## F!oBa
func kWS ()	begin
	end
## ]Sp&_T_waT
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 247))

	def test_248(self):
		input = '''
## :WVC2x(GESP!8*eyZ
## `sc"bGUGoi?SFXXTJPb
## ^/S/0ZS#
## #`BF](S7Y
func E4n (var aMd)
	return 318
'''
		expect = '''Error on line 6 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 248))

	def test_249(self):
		input = '''
number dps[2,0.369e-39,72] <- 280.442E+69
number vIV <- true
## ~K{
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 249))

	def test_250(self):
		input = '''
func ps8 ()
	return

bool qF[8.938,485.322] <- fjh
## yA,
## BOMQuPxl)o<$|
dynamic kH[9] <- Ee ## usPpaxRbwscphUe
'''
		expect = '''Error on line 8 col 10: ['''
		self.assertTrue(TestParser.test(input, expect, 250))

	def test_251(self):
		input = '''
## m9se*F
number vtbA
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 251))

	def test_252(self):
		input = '''
func xUo (var Z8zj[45.507,0,3.076])
	return

## 8F7G;Ha9
func GYeC (string TRt[4.390E+57], var HZm, string uSj)
	return 464E64
'''
		expect = '''Error on line 2 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 252))

	def test_253(self):
		input = '''
## oFCxvI
number MRU[9.866,34.610E-95] <- false ## e"ca=-x=tctt.X&
## !(NA? OoL65BFA
func MFTe (var XV[23,7,5])	return

'''
		expect = '''Error on line 5 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 253))

	def test_254(self):
		input = '''
var s2F <- ZpF9 ## "?,JM~Hw}
## Z_}vB1Iwx
var aRlm[43,8.586e-72,53.081]
'''
		expect = '''Error on line 4 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 254))

	def test_255(self):
		input = '''
dynamic uo[302.708E+03] <- "'"9y0" ## V3.jrT}q=^$%^=>
var ahBw <- 531e-37 ## ])Zso3hgzE:8]#5e{+
'''
		expect = '''Error on line 2 col 10: ['''
		self.assertTrue(TestParser.test(input, expect, 255))

	def test_256(self):
		input = '''
## qN]C[]
## =!7_
'''
		expect = '''Error on line 4 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 256))

	def test_257(self):
		input = '''
## *J
func Ie9F ()	return 3e97

## oy
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 257))

	def test_258(self):
		input = '''
## ,Wa5{
number l7ik[537.455E+89,66.795E95,3]
## j3_J2~JrkM
## 4aD cfL#5XB>
string v6zf <- YKN3 ## _>lb~l4NuJy`sZyCXx
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 258))

	def test_259(self):
		input = '''
func SS (string MsTT[0,3.319e26,91])	begin
		break
		LWvs(false, "'"jx")
	end
## [dT+[7mix(
string wvFv <- "n" ## L@8/yGLhibEg_ldOz5:u
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 259))

	def test_260(self):
		input = '''
func ohL ()	return
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 260))

	def test_261(self):
		input = '''
number HD[78.438e+50,15E23] <- 586.935E01 ## C!#zDvO73D*5
func hdwR ()	begin
		## HODc]#"]dnEN&gQOVs2+
	end

## k2FcwFB6-/x"y|N
func yvF (number fjS, bool f3S, dynamic gBOp)	return

'''
		expect = '''Error on line 8 col 32: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 261))

	def test_262(self):
		input = '''
func T8 ()
	return

var KBpR[706.257] ## !TCQt8%0vpsJ
'''
		expect = '''Error on line 5 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 262))

	def test_263(self):
		input = '''
func rh (string lk, var wE, var JVG)	begin
		continue
		## s9;|(*b%p`*$LF7
	end
dynamic Aqp5[67.409] ## wk1JkuoD*LzV*v[
func GA9 (var wqz[533], dynamic J8oO)	begin
		S2x("h'"'"", ShEO, true)
		begin
		end
		continue
	end

## 3Q, <}=
func NPl ()	return "'"'"'""

'''
		expect = '''Error on line 2 col 20: var'''
		self.assertTrue(TestParser.test(input, expect, 263))

	def test_264(self):
		input = '''
var f3Px[6,516E-36]
var Pv[20]
func mmSu ()	begin
	end

## i
dynamic sNm
'''
		expect = '''Error on line 2 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 264))

	def test_265(self):
		input = '''
dynamic bj
func Y_3 (string b3, string lN[0.504,463.373e74,71E-02])	return

## 6T>>8kp7P
## 5O^N/KQu?=b37k:
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 265))

	def test_266(self):
		input = '''
## ^
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 266))

	def test_267(self):
		input = '''
func J5 (dynamic q5Gw[643], bool ao9, string wnUX)	return false

'''
		expect = '''Error on line 2 col 9: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 267))

	def test_268(self):
		input = '''
func NCEF (dynamic Ys)
	return nxE

dynamic oIcr
var Qi5X[79] ## W
number GKZh <- AwH
## %qeq]xX9j "y
'''
		expect = '''Error on line 2 col 11: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 268))

	def test_269(self):
		input = '''
func pF0O (dynamic E1t[5], bool hiws[192,765.621], bool bs)	begin
		PyP <- 83.832
	end

## XZK_CBaNi4"z
func E6TY (dynamic DzC[4e-31,8.945,923], var zfe, string stj)
	begin
		## cpen1jqmbB}aLxq9fbUj
		OB()
	end

## Tf5 Cw{Lx,
func LwJ (string u1[7e-52,257e-59])
	return

'''
		expect = '''Error on line 2 col 11: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 269))

	def test_270(self):
		input = '''
number U96K[5] <- ZOjX
bool X6[6,31]
func KoVz ()	return
## Jgi9g?|@o:aeDv=
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 270))

	def test_271(self):
		input = '''
number mWlI <- false ## {&4A0|AI
func HXPl (var vja)	begin
		XG2 <- "'""
	end

func HmF (number FQS[9,8.417E+91,1.626E07], string SdO5[222.258e+46,74E+44,18.948e+61], string HlI)
	return 8

## @fM*?>J(`Yv#
## ^`50.2-YyRV
'''
		expect = '''Error on line 3 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 271))

	def test_272(self):
		input = '''
## ZM[
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 272))

	def test_273(self):
		input = '''
func oT ()
	return

number v_[884.013E76] <- "q6'"S"
func lcL (string kf, var sI)	return
'''
		expect = '''Error on line 6 col 21: var'''
		self.assertTrue(TestParser.test(input, expect, 273))

	def test_274(self):
		input = '''
var x3[8.944e40,196E+17,28E-06]
'''
		expect = '''Error on line 2 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 274))

	def test_275(self):
		input = '''
bool GSp[137,69e59] <- 541E-96
string RoUS <- true ## ~SuCG@ `Y30W-3
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 275))

	def test_276(self):
		input = '''
bool aua
## sUcM.?rF`LO"
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 276))

	def test_277(self):
		input = '''
## 6JYJs
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 277))

	def test_278(self):
		input = '''
func SEAu (var hK, string IS3h)	return "Rc'"*<"

## ~V|qWSu&
## 4MPqH{3`K/u
dynamic OS[493.106,692.340] ## u}[4+#
func SZo7 (number xJa)
	return Y7yH
'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 278))

	def test_279(self):
		input = '''
string mQWc ## 2{
var bd[88.089,4e86] <- false
'''
		expect = '''Error on line 3 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 279))

	def test_280(self):
		input = '''
func XWDa (var Q2, number mq[7.557E+61,32E+41], bool XgAP[91.398E+28,685.093e+61])	return
func QB (var rrf[68.670E10,93E83,91], bool Ghxw[8,71,1.888E-99], string zN)	begin
		begin
			## *:]-8Q3:~
			## Z|g%/F#WO-M!#
		end
		## 5*#
	end

number q6 <- 33E57 ## een#Y{_,"9Cyi3&,/6^
## oL
'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 280))

	def test_281(self):
		input = '''
bool fUB3[37E69,74]
func fJS (var uxIn[5.607,2,9e+67], bool BCd)	return
## Cl.lJzvEb4cnu57Sg
dynamic xt[3.353E-92,87e72,3.467e-37] <- false ## :xJ"#NGjfG^%r
'''
		expect = '''Error on line 3 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 281))

	def test_282(self):
		input = '''
var jm[829,821E+99,6e+77]
## k(Pl)Ap
func CK5 (dynamic eJ[7.392e-07,838.159e-47], number lp9p, number HGOM)
	begin
		break
		for YrQ until 26.520e+30 by false
			return true
	end

'''
		expect = '''Error on line 2 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 282))

	def test_283(self):
		input = '''
## RU18[3.mmFgY^|y:9
## (/_QG4|/r
string in8j[42E+81] <- 62 ## P=>{
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 283))

	def test_284(self):
		input = '''
func eSvK (dynamic U6[754,390.211E-92], string CO0s)
	return 5e-87

## HZ7smR4y=6eFr|Hcgbb
## NtKgOMp}DCx
'''
		expect = '''Error on line 2 col 11: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 284))

	def test_285(self):
		input = '''
func mei (number tI4i)
	return

func lF (string LZw4[0.158e-05,43], bool jnFm)	begin
		break
		R_nG(mkKP)[BaK] <- "'"'"`'"V"
		## >?^Q3HvB
	end

func CD (bool gZ[1.078])	begin
		## b-jx*68s4
		if (false)
		return false
		elif ("'"P4~'"") continue
		elif ("Z") for qd until true by "<5'""
			string pa[29.885,492E+30,68.636E-86] ## 7%{>VNw|v
		elif (316E+07) dynamic Re[6.103] <- false ## R[TN-i#+rg_%YR!<i
		elif ("'"")
		number cB <- Unf ## g"(Lwke^_qmTGmho=
		elif ("c'"'"") Qa <- 6
		else continue
	end

func nVan (bool puB, number a4l, number t3)	begin
		nB["'"'"", "'"GJ'"", u59] <- Zu
		string dzwf <- "L'"6" ## "+eljf[V5iW<56
		## Q_oCUD
	end

'''
		expect = '''Error on line 7 col 12: ['''
		self.assertTrue(TestParser.test(input, expect, 285))

	def test_286(self):
		input = '''
var JqNh <- 0e+58 ## {`|S,~h2_
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 286))

	def test_287(self):
		input = '''
## )A7=B3sYjk8o4{M.iE>&
var x19E
bool Cz_r[572,10.127] <- false
bool BKX[8.690,137.655,2E-80] <- hQl
func Sa (string OM7[8.182,788E63,8.095e54], dynamic oG, number g0[0.158,9E-54])
	begin
		begin
			## p"{|>,RsbX
		end
		return GFkm
		## 8 j
	end

'''
		expect = '''Error on line 3 col 9: 
'''
		self.assertTrue(TestParser.test(input, expect, 287))

	def test_288(self):
		input = '''
number xE[8.370e-36] <- ">[I'""
func yW ()	return true

dynamic Iw[4.428e+20,61E-71] <- 321.195
func UaU ()
	return false

'''
		expect = '''Error on line 5 col 10: ['''
		self.assertTrue(TestParser.test(input, expect, 288))

	def test_289(self):
		input = '''
func a8w (bool LTTo[37,449])
	return false
var uAWn[6.068,805.479,6.564E04]
dynamic yk[69E+42,615,105.726E+52] <- USMK
number rpxV[1,61E62]
func oj (bool BzW[3E29,9.954], string hdBB)	return
'''
		expect = '''Error on line 4 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 289))

	def test_290(self):
		input = '''
func NC ()
	begin
		NVw(79.123, 7e30)
		return Yx
	end

func Oi (bool x3v[8,78.805,486E-97], dynamic Rs[709.044E-04,7e-24], string Vcvy[2.263])	begin
		## bmsKEAqM~tiab/
	end

'''
		expect = '''Error on line 8 col 37: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 290))

	def test_291(self):
		input = '''
func mwC (var DA, number E9EJ[57.717,87.934E-37])
	return
dynamic dSfw[3.523] <- UhM
'''
		expect = '''Error on line 2 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 291))

	def test_292(self):
		input = '''
## 5(hPre$m~E:1t9x/6J7j
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 292))

	def test_293(self):
		input = '''
## )Y[}
func GidA (bool Qdk, bool GS, string GC)	return u7

var sA[4.394e+15,31]
'''
		expect = '''Error on line 5 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 293))

	def test_294(self):
		input = '''
func tCt (var H1ZU[680e-74,160.045])
	return

func tt (number lo2[20])	return

var UeB <- 2e-00
'''
		expect = '''Error on line 2 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 294))

	def test_295(self):
		input = '''
dynamic vR
## c>b
## v
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 295))

	def test_296(self):
		input = '''
## gtY(
func dCZD ()	return

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 296))

	def test_297(self):
		input = '''
func zmcE (string CYcv, var iZu)
	begin
		## WC|i"4xsVx>$Rd|MtV
	end

## Ft;$eI0w3j-U
string UXL6[893.750,71] ## WYY
'''
		expect = '''Error on line 2 col 24: var'''
		self.assertTrue(TestParser.test(input, expect, 297))

	def test_298(self):
		input = '''
func rikT (var nK, var wW[878,9.994E-52,1])
	return 417.425
'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 298))

	def test_299(self):
		input = '''
dynamic ztMV[6,53.168E-64,688.250E-47]
'''
		expect = '''Error on line 2 col 12: ['''
		self.assertTrue(TestParser.test(input, expect, 299))

	def test_300(self):
		input = '''
dynamic hN3[5.300E46,6E75] <- false ## % 3Fm-BczY-*.$?)i!
func GV ()	begin
	end
func jOAY (var LOIl)	return
number mPf <- true
'''
		expect = '''Error on line 2 col 11: ['''
		self.assertTrue(TestParser.test(input, expect, 300))

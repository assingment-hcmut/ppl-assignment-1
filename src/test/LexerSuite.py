import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
	def test_101(self):
		self.assertTrue(TestLexer.test("MjBKjvgKiU", "MjBKjvgKiU,<EOF>", 101))

	def test_102(self):
		self.assertTrue(TestLexer.test("0", "0,<EOF>", 102))

	def test_103(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 103))

	def test_104(self):
		self.assertTrue(TestLexer.test('''## NOH=07r''', '''<EOF>''', 104))

	def test_105(self):
		self.assertTrue(TestLexer.test("167e-01", "167e-01,<EOF>", 105))

	def test_106(self):
		self.assertTrue(TestLexer.test('''"{\\g'"t'""''', '''Illegal Escape In String: {\\g''', 106))

	def test_107(self):
		self.assertTrue(TestLexer.test("49e56", "49e56,<EOF>", 107))

	def test_108(self):
		self.assertTrue(TestLexer.test('''"G'" ''', '''Unclosed String: G'" ''', 108))

	def test_109(self):
		self.assertTrue(TestLexer.test('''"'"'"'"\\hw"''', '''Illegal Escape In String: '"'"'"\\h''', 109))

	def test_110(self):
		self.assertTrue(TestLexer.test("48.630E82", "48.630E82,<EOF>", 110))

	def test_111(self):
		self.assertTrue(TestLexer.test('''## #,''', '''<EOF>''', 111))

	def test_112(self):
		self.assertTrue(TestLexer.test('''## Sji''', '''<EOF>''', 112))

	def test_113(self):
		self.assertTrue(TestLexer.test('''"'"'""''', '''\'"'",<EOF>''', 113))

	def test_114(self):
		self.assertTrue(TestLexer.test("Fb", "Fb,<EOF>", 114))

	def test_115(self):
		self.assertTrue(TestLexer.test("#2lWTSqvUD", "Error Token #", 115))

	def test_116(self):
		self.assertTrue(TestLexer.test("147.151", "147.151,<EOF>", 116))

	def test_117(self):
		self.assertTrue(TestLexer.test('''"W ''', '''Unclosed String: W ''', 117))

	def test_118(self):
		self.assertTrue(TestLexer.test("5E-35", "5E-35,<EOF>", 118))

	def test_119(self):
		self.assertTrue(TestLexer.test('''"'"'"'"~\\h'""''', '''Illegal Escape In String: '"'"'"~\\h''', 119))

	def test_120(self):
		self.assertTrue(TestLexer.test('''## ;2n0;J247%p~*''', '''<EOF>''', 120))

	def test_121(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 121))

	def test_122(self):
		self.assertTrue(TestLexer.test("8.132E+32", "8.132E+32,<EOF>", 122))

	def test_123(self):
		self.assertTrue(TestLexer.test('''"'"$'"p"''', '''\'"$'"p,<EOF>''', 123))

	def test_124(self):
		self.assertTrue(TestLexer.test('''## }$yHFYE+eNr~}_[''', '''<EOF>''', 124))

	def test_125(self):
		self.assertTrue(TestLexer.test("@4l91", "Error Token @", 125))

	def test_126(self):
		self.assertTrue(TestLexer.test('''"@B'"'""''', '''@B'"'",<EOF>''', 126))

	def test_127(self):
		self.assertTrue(TestLexer.test("25.459e+30", "25.459e+30,<EOF>", 127))

	def test_128(self):
		self.assertTrue(TestLexer.test('''## Utht:82p+J>$sUu4o|H''', '''<EOF>''', 128))

	def test_129(self):
		self.assertTrue(TestLexer.test('''"'"y\\x'"0"''', '''Illegal Escape In String: '"y\\x''', 129))

	def test_130(self):
		self.assertTrue(TestLexer.test("s3Pc274E1", "s3Pc274E1,<EOF>", 130))

	def test_131(self):
		self.assertTrue(TestLexer.test("DJMtVixLz4", "DJMtVixLz4,<EOF>", 131))

	def test_132(self):
		self.assertTrue(TestLexer.test('''## `hLc"N''', '''<EOF>''', 132))

	def test_133(self):
		self.assertTrue(TestLexer.test("iOpi_Vx", "iOpi_Vx,<EOF>", 133))

	def test_134(self):
		self.assertTrue(TestLexer.test("11.976e-69", "11.976e-69,<EOF>", 134))

	def test_135(self):
		self.assertTrue(TestLexer.test('''## Ln<L_qZD|P.aa''', '''<EOF>''', 135))

	def test_136(self):
		self.assertTrue(TestLexer.test('''## P#R''', '''<EOF>''', 136))

	def test_137(self):
		self.assertTrue(TestLexer.test("tefW5@A@", "tefW5,Error Token @", 137))

	def test_138(self):
		self.assertTrue(TestLexer.test('''"^"''', '''^,<EOF>''', 138))

	def test_139(self):
		self.assertTrue(TestLexer.test('''## _6|9Is BS09UF*jo,@''', '''<EOF>''', 139))

	def test_140(self):
		self.assertTrue(TestLexer.test('''## }''', '''<EOF>''', 140))

	def test_141(self):
		self.assertTrue(TestLexer.test('''## Y2[-}g_"`"*+DFUP+~S{''', '''<EOF>''', 141))

	def test_142(self):
		self.assertTrue(TestLexer.test("488E31", "488E31,<EOF>", 142))

	def test_143(self):
		self.assertTrue(TestLexer.test("j8fK", "j8fK,<EOF>", 143))

	def test_144(self):
		self.assertTrue(TestLexer.test('''"D'"'""''', '''D'"'",<EOF>''', 144))

	def test_145(self):
		self.assertTrue(TestLexer.test('''## #%hen }9fz W_G''', '''<EOF>''', 145))

	def test_146(self):
		self.assertTrue(TestLexer.test("73", "73,<EOF>", 146))

	def test_147(self):
		self.assertTrue(TestLexer.test('''"'"?9'""''', '''\'"?9'",<EOF>''', 147))

	def test_148(self):
		self.assertTrue(TestLexer.test("55.244", "55.244,<EOF>", 148))

	def test_149(self):
		self.assertTrue(TestLexer.test("_Ok5xg#16", "_Ok5xg,Error Token #", 149))

	def test_150(self):
		self.assertTrue(TestLexer.test('''## f1~S&jX=''', '''<EOF>''', 150))

	def test_151(self):
		self.assertTrue(TestLexer.test("jAz0M", "jAz0M,<EOF>", 151))

	def test_152(self):
		self.assertTrue(TestLexer.test("667E38", "667E38,<EOF>", 152))

	def test_153(self):
		self.assertTrue(TestLexer.test('''"'"
<"''', '''Unclosed String: '"''', 153))

	def test_154(self):
		self.assertTrue(TestLexer.test('''## ]swO{V.>BGvn$mEw''', '''<EOF>''', 154))

	def test_155(self):
		self.assertTrue(TestLexer.test('''## ?F*(L''', '''<EOF>''', 155))

	def test_156(self):
		self.assertTrue(TestLexer.test("08$6u^", "08,Error Token $", 156))

	def test_157(self):
		self.assertTrue(TestLexer.test('''## {{''', '''<EOF>''', 157))

	def test_158(self):
		self.assertTrue(TestLexer.test("95.552e-70", "95.552e-70,<EOF>", 158))

	def test_159(self):
		self.assertTrue(TestLexer.test("83.219E99", "83.219E99,<EOF>", 159))

	def test_160(self):
		self.assertTrue(TestLexer.test('''## 6''', '''<EOF>''', 160))

	def test_161(self):
		self.assertTrue(TestLexer.test("Y5", "Y5,<EOF>", 161))

	def test_162(self):
		self.assertTrue(TestLexer.test('''## k$qJEl`t''', '''<EOF>''', 162))

	def test_163(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 163))

	def test_164(self):
		self.assertTrue(TestLexer.test('''## 1 6{2s;:D.gx?zRB]&m''', '''<EOF>''', 164))

	def test_165(self):
		self.assertTrue(TestLexer.test("gLiPy^j", "gLiPy,Error Token ^", 165))

	def test_166(self):
		self.assertTrue(TestLexer.test('''"3'"h'" ''', '''Unclosed String: 3'"h'" ''', 166))

	def test_167(self):
		self.assertTrue(TestLexer.test('''## sdaYvl$$nk2.:''', '''<EOF>''', 167))

	def test_168(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 168))

	def test_169(self):
		self.assertTrue(TestLexer.test('''"'"^'"	 ''', '''Unclosed String: '"^'"	 ''', 169))

	def test_170(self):
		self.assertTrue(TestLexer.test('''"Z
'""''', '''Unclosed String: Z''', 170))

	def test_171(self):
		self.assertTrue(TestLexer.test('''"'"0"''', '''\'"0,<EOF>''', 171))

	def test_172(self):
		self.assertTrue(TestLexer.test('''## {~~xz;"V/p''', '''<EOF>''', 172))

	def test_173(self):
		self.assertTrue(TestLexer.test('''"'"'"'" ''', '''Unclosed String: '"'"'" ''', 173))

	def test_174(self):
		self.assertTrue(TestLexer.test("hU#Ax6CQ", "hU,Error Token #", 174))

	def test_175(self):
		self.assertTrue(TestLexer.test("183", "183,<EOF>", 175))

	def test_176(self):
		self.assertTrue(TestLexer.test("8R7@GHMpno", "8,R7,Error Token @", 176))

	def test_177(self):
		self.assertTrue(TestLexer.test('''## p{KJ&7F>k3U,?n6qJ''', '''<EOF>''', 177))

	def test_178(self):
		self.assertTrue(TestLexer.test('''"~m'"8 ''', '''Unclosed String: ~m'"8 ''', 178))

	def test_179(self):
		self.assertTrue(TestLexer.test("JX_$zh@", "JX_,Error Token $", 179))

	def test_180(self):
		self.assertTrue(TestLexer.test('''## |''', '''<EOF>''', 180))

	def test_181(self):
		self.assertTrue(TestLexer.test('''"+'"
['"'""''', '''Unclosed String: +'"''', 181))

	def test_182(self):
		self.assertTrue(TestLexer.test("q3", "q3,<EOF>", 182))

	def test_183(self):
		self.assertTrue(TestLexer.test('''"
l"''', '''Unclosed String: ''', 183))

	def test_184(self):
		self.assertTrue(TestLexer.test("4e26", "4e26,<EOF>", 184))

	def test_185(self):
		self.assertTrue(TestLexer.test("8", "8,<EOF>", 185))

	def test_186(self):
		self.assertTrue(TestLexer.test('''##  <''', '''<EOF>''', 186))

	def test_187(self):
		self.assertTrue(TestLexer.test("4.044e-05", "4.044e-05,<EOF>", 187))

	def test_188(self):
		self.assertTrue(TestLexer.test("2e69", "2e69,<EOF>", 188))

	def test_189(self):
		self.assertTrue(TestLexer.test("GD", "GD,<EOF>", 189))

	def test_190(self):
		self.assertTrue(TestLexer.test('''## x8CqA''', '''<EOF>''', 190))

	def test_191(self):
		self.assertTrue(TestLexer.test('''## TT/&Bokr''', '''<EOF>''', 191))

	def test_192(self):
		self.assertTrue(TestLexer.test("9E08", "9E08,<EOF>", 192))

	def test_193(self):
		self.assertTrue(TestLexer.test("3", "3,<EOF>", 193))

	def test_194(self):
		self.assertTrue(TestLexer.test("sQfOsaf^SZ", "sQfOsaf,Error Token ^", 194))

	def test_195(self):
		self.assertTrue(TestLexer.test('''## iYWBf4t@Qy,''', '''<EOF>''', 195))

	def test_196(self):
		self.assertTrue(TestLexer.test('''## 4 CG jT0`2@M&8OO.,D''', '''<EOF>''', 196))

	def test_197(self):
		self.assertTrue(TestLexer.test("9", "9,<EOF>", 197))

	def test_198(self):
		self.assertTrue(TestLexer.test('''## wG#pq~i^gEob(5O;WU''', '''<EOF>''', 198))

	def test_199(self):
		self.assertTrue(TestLexer.test("d6T", "d6T,<EOF>", 199))

	def test_200(self):
		self.assertTrue(TestLexer.test("4E87", "4E87,<EOF>", 200))
